import { Header } from "antd/es/layout/layout";
import { useNavigate } from "react-router-dom";
import { FaHome, FaSignOutAlt, FaUserCircle, FaUserFriends } from "react-icons/fa";
import { MdQuiz } from "react-icons/md";
import { Menu } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { removeAccessToken } from "../../utils/storage.util";
import { removeUser } from "../../redux/slice/user.slice.js";
import { rememberUser } from "../../redux/slice/option.slice.js";
import { useEffect } from "react";

export const AppHeader = () => {
    const dispatch = useDispatch();
    const user = useSelector((state) => state.user);

    const navigate = useNavigate();

    const handleLogout = () => {
        removeAccessToken();
        dispatch(removeUser({}));
        dispatch(rememberUser({ remember: false }));
        navigate("/");
    };

    const handleHome = () => {
        if (user.role === 1) {
            navigate("/teacher/home");
        } else if (user.role === 2) {
            navigate("/student/home");
        } else navigate("/");
    };

    const handleCreateClass = () => {
        navigate("/teacher/classes/create");
    };

    const handleGetListClass = () => {
        navigate("/teacher/classes");
    };
    const handleGetListExam = () => {
        navigate("/teacher/quizzes/list");
    };

    const handleCreateTest = () => {
        navigate("/teacher/quizzes/create-quiz");
    };

    const leftItems = [
        {
            key: "/home",
            label: <a onClick={handleHome}>Trang chủ</a>,
            icon: <FaHome />,
        },
        {
            key: "/class",
            label: "Lớp học",
            children: [
                {
                    key: "/people/list",
                    label: (
                        <a href="" onClick={handleGetListClass}>
                            Danh sách lớp học
                        </a>
                    ),
                },
                {
                    key: "/people/create",
                    label: <a onClick={handleCreateClass}>Thêm lớp học mới</a>,
                },
            ],
            icon: <FaUserFriends />,
        },
        {
            key: "/quiz",
            label: "Bài kiểm tra",
            children: [
                {
                    key: "/quiz/list",
                    label: <a onClick={handleGetListExam}>Danh sách bài kiểm tra</a>,
                },
                {
                    key: "/quiz/create",
                    label: <a onClick={handleCreateTest}>Thêm bài kiểm tra mới</a>,
                },
            ],
            icon: <MdQuiz />,
        },
    ];

    const rightItems = [
        {
            key: "/user",
            label: `${user?.email}`,
            icon: <FaUserCircle />,
            children: [
                {
                    key: "user/manage",
                    label: <a>Quản lý tài khoản</a>,
                },
                {
                    key: "user/logout",
                    label: (
                        <a href="" onClick={handleLogout}>
                            Đăng xuất
                        </a>
                    ),
                    icon: <FaSignOutAlt />,
                },
            ],
        },
    ];

    if (!user) {
        navigate("/");
        return;
    }

    return (
        <Header
            style={{
                position: "sticky",
                top: 0,
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
            }}
        >
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]} items={leftItems} />
            <Menu theme="dark" mode="horizontal" items={rightItems} />
        </Header>
    );
};
