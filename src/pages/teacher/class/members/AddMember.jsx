import { Input, Modal, message, List, Button } from "antd";
import { useEffect, useMemo, useState } from "react";
import { debounce } from "lodash";
import { addMember, getUsers } from "../../../../request/class-members.request.js";

export const AddMember = ({ isAddingMember, closeAddMemberModal, id, addNewMember }) => {
    const [queryUser, setQueryUser] = useState("");
    const [users, setUsers] = useState([]);

    const filteredUsers = useMemo(() => {
        if (!queryUser) return users;
        const filter = users.filter((user) => {
            return `${user.number}: ${user.firstName} ${user.lastName}`.toUpperCase().includes(queryUser.toUpperCase());
        });
        return filter;
    }, [queryUser]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const users = await getUsers();
                setUsers(
                    users.records.map((user) => ({
                        ...user,
                        fullName: `${user.firstName}  ${user.lastName}`,
                    }))
                );
            } catch (error) {
                console.log(error);
            }
        };
        fetchData();
    }, []);

    const handleAddNewMember = async (user) => {
        await addMember({ classId: Number(id), userId: Number(user.id) })
            .then((result) => {
                addNewMember(user);
                message.success("Thêm thành công");
            })
            .catch((error) => {
                console.log(error);
            });
    };

    return (
        <>
            <Modal title="Thêm thành viên" open={isAddingMember} onCancel={closeAddMemberModal} footer={null}>
                <Input.Search
                    allowClear
                    size="default"
                    placeholder="Tìm kiếm"
                    enterButton
                    onChange={debounce((e) => setQueryUser(e?.target?.value), 400)}
                />

                <List
                    dataSource={filteredUsers}
                    renderItem={(user) => (
                        <List.Item key={user.email}>
                            <List.Item.Meta
                                title={`${user.firstName} ${user.lastName}${` ${user.number}`}`}
                                description={user.email}
                            />
                            <Button onClick={() => handleAddNewMember(user)}>Thêm</Button>
                        </List.Item>
                    )}
                />
            </Modal>
        </>
    );
};
